from smbus2 import SMBus
import project_defaults

# Open i2c bus 1 and read one byte from address 80, offset 0
bus = SMBus(1)

b = bus.read_byte_data(project_defaults.I2C_SLAVE_ADDRESS, 0xAA)
# print("sent to %.2X offset 0xAA received %.2X" % (project_defaults.I2C_SLAVE_ADDRESS, b))
print("Status received %.2X" % (b))

bus.close()
