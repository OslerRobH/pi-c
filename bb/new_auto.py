import smbus
import time

INITIAL_COMMAND = 3
LAST_COMMAND = 9

bus = smbus.SMBus(1)
j = 0
k = 0
command = 0xE5
value1 = INITIAL_COMMAND
value2 = 0

def sendData(slaveAddress, data):
    global j 
    intsOfData = list(map(ord, data))
    try:
        bus.write_i2c_block_data(slaveAddress, intsOfData[0], intsOfData[1:])
        print("PASS")

    except:
        j = j + 1
        mod = j % 10
        if mod == 1:
            print("%d failures" % (j))

while True:
     
    values = "%c%c%c" % (command, value1, value2)
    sendData(0x03, values)
    if k == 2000:
        # increment command Value 1
        value1 = value1 + 1
        if value1 == LAST_COMMAND:
            value1 = INITIAL_COMMAND
    else:
        # retry sending
        k = k + 1
        time.sleep(0.1)
