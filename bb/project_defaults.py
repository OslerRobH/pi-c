'''
This is a project wide set of default values that are used in multiple places
'''

I2C_SLAVE_ADDRESS = 0x03
I2C_SLAVE_READ_ADDRESS = 0x83
I2C_MASTER_ADDRESS = 0x00

COMMAND_MESSAGE_TYPE = 0xE5
STATUS_MESSAGE_TYPE = 0xE6
