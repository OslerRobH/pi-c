'''
This is a message sender it sends the I2C message to the Slave Address
'''
import smbus
import time
import project_defaults

INITIAL_COMMAND = 1
LAST_COMMAND = 6

bus = smbus.SMBus(1)
j = 0
k = 0
command = 0xE5
value1 = INITIAL_COMMAND
value2 = 0

def sendData(slaveAddress, data):
    global j 
    intsOfData = list(map(ord, data))
    try:
        bus.write_i2c_block_data(slaveAddress, intsOfData[0], intsOfData[1:])
        # print("PASS")
        return True

    except:
        j = j + 1
        mod = j % 10
        if mod == 1:
            print("%d failures Value1 = %d Value2 = %d" % (j, intsOfData[0], intsOfData[1]))
        return False

# this cycles through commands from INITIAL_COMMAND to 1 less than LAST_COMMAND
# for up to 30 times then steps on to the next command ...

while True:
     
    values = "%c%c%c" % (command, value1, value2)
    result = sendData(project_defaults.I2C_SLAVE_ADDRESS, values)
    # print ("k = %d" % (k))
    if k == 30:
        k = 0
        # increment command Value 1
        value1 = value1 + 1
        if value1 == LAST_COMMAND:
            value1 = INITIAL_COMMAND
    else:
        # retry sending
        k = k + 1
        time.sleep(0.1)
