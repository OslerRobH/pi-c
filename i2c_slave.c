#include <pigpio.h>
#include <iostream>
#include <atomic>
#include <signal.h>
#include <memory.h>

//g++ -Wall -pthread -o test test.cpp -lpigpio -lrt

std::atomic<bool> Quit = ATOMIC_VAR_INIT(false);

void quit(int sig)
{
    Quit = true; //will require handler quit too
}

int main(int argc, char** argv)
{
    signal(SIGINT, quit);

    bsc_xfer_t xfer;
    gpioInitialise();

    xfer.control = (0x0A<<16) | 0x305; 

    int status = bscXfer(&xfer);
    if (status >= 0)
    {
        xfer.rxCnt = 0;

        while(!Quit)
        {
            status = bscXfer(&xfer);
            if(status)
            {
                if (xfer.rxCnt > 0)
                {
                    std::cout << std::endl << xfer.rxBuf;
                    memset( xfer.rxBuf, '\0', sizeof(char)*BSC_FIFO_SIZE );
                }
                else
                {
                    //std::cout<<"\n No datas";
                }
            }
        }
    }
    else
    {
        std::cout<<"\nInit status error.\n"<<std::flush;
    }
    return status;
}
