#!/usr/bin/env python

# 2014-08-26 i2c_master.py

import time
import sys
import pigpio

result = 0

# sudo pigpiod
# python3 i2c_master.py

# Connect Pi <--> Pi, Ground - Ground, SDA - SDA, SCL - SCL.

SDA=18
SCL=19
I2C_MASTER_ADDRESS=0x09
BUS=1

print("i2c_master.py start up")
pi = pigpio.pi() # Connect to local Pi.

# Add pull-ups in case external pull-ups haven't been added
# pi.set_pull_up_down(SDA, pigpio.PUD_UP)
# pi.set_pull_up_down(SCL, pigpio.PUD_UP)

v = pi.get_pigpio_version()
print("i2c_master.py pigpio version = %s" % (v))

handle = pi.i2c_open(BUS, I2C_MASTER_ADDRESS)

if handle < 0:
    print("i2c_master.py FAIL handle=%x" % (handle))
    exit(1)


if handle >= 0:
    looper = True
    print("i2c_master.py PASS handle=%x" % (handle))

# while True == looper:
# 
#     aout = 0
#     for a in range(0,10):
#         # worker try:
#              # worker aout = aout + 1
#              # result = pi.i2c_write_byte_data(handle, I2C_MASTER_ADDRESS | ((a+1) & 0x03), aout&0xFF)
#              # result = pi.i2c_write_block_data(handle, 0, b'hello')
#              # worker result = pi.i2c_write_block_data(handle, 0, b'\xE5\x00\x00')
#              # print("i2c_master.py write_byte_data=%x PASS\n" % (result))
#              # result = pi.i2c_write_byte(handle, 0x0)
#              # worker print("i2c_master.py iteration=%x PASS\n" % (a))
#              # worker looper = False
#              # worker break
#         # worker except:
#             # print('i2c_write_byte iteration=%x RETRY\n' % (a))
#             # worker pass
#             # worker time.sleep(1)
# 
#         # worker if a == 9:
#         # worker     print("FATAL retries exceeded") 
#         # worker time.sleep(1)
#           
# #           break  
# #      else:
#            # try:
#            #    # v = pi.i2c_read_byte(handle)
#            #    # print("i2c_master.py i2c_read_byte=%x PASS\n"  % (v))
#            #    aout = aout + 1
#            #    #v = pi.i2c_write_byte_data(handle, I2C_MASTER_ADDRESS | ((a+1) & 0x03), aout&0xFF)
#            # v = pi.i2c_write_byte(1, 17)
# 
#            pi.i2c_write_byte_data(handle, 0, 1) 
#            print("i2c_master.py i2c_write_byte PASS")
# 
#            #   v = pi.i2c_read_byte(handle)
#            #   print("i2c_master.py i2c_read_byte=%x PASS\n"  % (v))
# 
#            # except:
#            #    # print('i2c_read_byte function FAIL')
#            #    print('i2c_write_byte function FAIL')
#            time.sleep(1)
       

#         if(pygame.key.get_pressed()[pygame.K_SPACE] != 0):
#             break
#             looper = False

v = pi.i2c_close(handle)
if v > 0:
    print("i2c_master.py i2c_close FAIL v = %x" % (v))
else:
    print("i2c_master.py i2c_close PASS v = %x" % (v))

pi.stop()

print("i2c_master.py shut down\n")

