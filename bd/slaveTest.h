/**
  * @file slaveTest.h
  * @brief this is the definition file for the implementation file of the same 
  *        name slaveTest.cpp.
  *        Casting Class for receiving and validating a Command message
  */
  
//  #include "pack_on.h"
//  #include "pack_off.h"
  
//  PACKED_CLASS
  
  class CommandResponse {
  private:
    uint8_t m_command;
    uint8_t m_value1;
    uint8_t m_value2;
    
  public:
    CommandResponse() { m_command =  m_value1 = m_value2 = 0; }
    uint8_t get_command() { return m_command; }
    uint8_t get_value1()  { return m_value1; }
    uint8_t get_value2()  { return m_value2; }

    uint8_t set_command(const uint8_t newCommand) { m_command = newCommand; return m_command; }
    uint8_t set_value1(const uint8_t newValue1)  { m_value1 = newValue1; return m_value1; }
    uint8_t set_value2(const uint8_t newValue2)  { m_value2 = newValue2; return m_value2; }
    
    bool IsCommandValid() { return (m_command == 0xE5); }
    
  } CommandResponse_t;

//PACK_END
