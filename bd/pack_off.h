#ifdef _MSC_VER
#pragma pack(1)
#define PACKED_STRUCTURE /* nothing */
#endif

#ifdef __GNUC__
#define PACKED_STRUCTURE __attribute__(packed)
#define PACKED_END }
#endif