/**
  * @file slaveTest.cpp
  * @brief This application runs as an I2C Slave and receives messages from the 
  *        Python I2C Master.
  *        This follows an aborted attempt at getting Python to act as a Slave.
  *        Enhancement #1 
  *          Get the received i2C packet to be checked for length and content 
  *          all Command messages shall begin with 0xE5. 
  *          Refer to the Code Separation document Command - Response Table.
  *          If valid cast it to a structure, need to pass this structure into 
  *          Python and then run through a pseudo Command decode.  
  *          At this point the code is ready for merging into pi-split Slave 
  *          project.
  */

#include <pigpio.h>


#include <stdio.h>

void runSlave();
void closeSlave();
int getControlBits(int, uint8_t);

const int slaveAddress = 0x03; // <-- Your address of choice
bsc_xfer_t xfer; // Struct to control data flow

uint8_t Command = 0;
uint8_t Value1 = 0;
uint8_t Value2 = 0;

int main(){
    // printf("main start\n");

    // Chose one of those two lines (comment the other out):
    runSlave();
    //closeSlave();

    // printf("main end\n");
    return 0;
}

void runSlave() {

    (void) gpioInitialise();
    printf("Initialized GPIOs\n");
	
    // Close old device (if any)
    xfer.control = getControlBits(slaveAddress, 0 /*false*/); // To avoid conflicts when restarting

    bscXfer(&xfer);
    // Set I2C slave Address 
    xfer.control = getControlBits(slaveAddress, 1 /* true */);
    int status = bscXfer(&xfer); // Should now be visible in I2C-Scanners

    if (status >= 0)
    {
        printf("Opened slave\n");
        xfer.rxCnt = 0;
        while(Command == 0)
        {
            bscXfer(&xfer);
            if(xfer.rxCnt > 0) {
                if (xfer.rxCnt == 0x03)
                {
                    if (0xE5 == xfer.rxBuf[0]) 
                    {
                        Command = xfer.rxBuf[0];
                        Value1  = xfer.rxBuf[1];
                        Value2  = xfer.rxBuf[2];
                        // printf("VALID %.2X %.2X %.2X", Command, Value1, Value2);
                    }
                    else
                    {
                        printf("INVALID Command [%.2X %.2X %.2X]", Command, Value1, Value2);
                    }
                }
                else
                {
                    printf("DOES NOT match the size of a Command %.2X : %.2X", 0x03, xfer.rxCnt);
                }
            }
        }
    }else
        printf("Failed to open slave!!!\n");

    // printf("leaving runSlave()");
}

void closeSlave() {
    gpioInitialise();
    printf("Initialized GPIOs\n");

    xfer.control = getControlBits(slaveAddress, 0 /* false */);
    bscXfer(&xfer);
    printf("Closed slave.\n");

    gpioTerminate();
    printf("Terminated GPIOs.\n");
}


int getControlBits(int address /* max 127 */, uint8_t open) {
    /*
    Excerpt from http://abyz.me.uk/rpi/pigpio/cif.html#bscXfer regarding the control bits:

    22 21 20 19 18 17 16 15 14 13 12 11 10 09 08 07 06 05 04 03 02 01 00
    a  a  a  a  a  a  a  -  -  IT HC TF IR RE TE BK EC ES PL PH I2 SP EN

    Bits 0-13 are copied unchanged to the BSC CR register. See pages 163-165 of the Broadcom 
    peripherals document for full details. 

    aaaaaaa defines the I2C slave address (only relevant in I2C mode)
    IT  invert transmit status flags
    HC  enable host control
    TF  enable test FIFO
    IR  invert receive status flags
    RE  enable receive
    TE  enable transmit
    BK  abort operation and clear FIFOs
    EC  send control register as first I2C byte
    ES  send status register as first I2C byte
    PL  set SPI polarity high
    PH  set SPI phase high
    I2  enable I2C mode
    SP  enable SPI mode
    EN  enable BSC peripheral
    */

    // Flags like this: 0b/*IT:*/0/*HC:*/0/*TF:*/0/*IR:*/0/*RE:*/0/*TE:*/0/*BK:*/0/*EC:*/0/*ES:*/0/*PL:*/0/*PH:*/0/*I2:*/0/*SP:*/0/*EN:*/0;

    int flags;
    if(open)
        flags = /*RE:*/ (1 << 9) | /*TE:*/ (1 << 8) | /*I2:*/ (1 << 2) | /*EN:*/ (1 << 0);
    else // Close/Abort
        flags = /*BK:*/ (1 << 7) | /*I2:*/ (0 << 2) | /*EN:*/ (0 << 0);

    return (address << 16 /*= to the start of significant bits*/) | flags;
}

uint8_t get_Command(void)
{
    return Command;
}

uint8_t get_Value1(void)
{
    return Value1;
}

uint8_t get_Value2(void)
{
    return Value2;
}
