/**
  * @file slaveTest.cpp
  * @brief This application runs as an I2C Slave and receives messages from the 
  *        Python I2C Master.
  *        This follows an aborted attempt at getting Python to act as a Slave.
  *        Enhancement #1 
  *          Get the received i2C packet to be checked for length and content 
  *          all Command messages shall begin with 0xE5. 
  *          Refer to the Code Separation document Command - Response Table.
  *          If valid cast it to a structure, need to pass this structure into 
  *          Python and then run through a pseudo command decode.  
  *          At this point the code is ready for merging into pi-split Slave 
  *          project.
  */

#include <pigpio.h>
#include <iostream>
#include "slaveTest.h"

using namespace std;

void runSlave();
void closeSlave();
int getControlBits(int, bool);

const int slaveAddress = 0x03; // <-- Your address of choice
bsc_xfer_t xfer; // Struct to control data flow
CommandResponse * CR;


int main(){
    CR = new CommandResponse();

    // Chose one of those two lines (comment the other out):
    runSlave();
    //closeSlave();

    delete CR;
    return 0;
}

void runSlave() {
    gpioInitialise();
    cout << "Initialized GPIOs\n";
    // Close old device (if any)
    xfer.control = getControlBits(slaveAddress, false); // To avoid conflicts when restarting
    bscXfer(&xfer);
    // Set I2C slave Address 
    xfer.control = getControlBits(slaveAddress, true);
    int status = bscXfer(&xfer); // Should now be visible in I2C-Scanners

    if (status >= 0)
    {
        cout << "Opened slave\n";
        xfer.rxCnt = 0;
        while(1){
            bscXfer(&xfer);
            if(xfer.rxCnt > 0) {
                // cout << "Received " << xfer.rxCnt << " bytes: ";
                if (xfer.rxCnt == sizeof(CommandResponse))
                {
                    // cout << "matches the size of a CommandResponse";
                    CR->set_command(xfer.rxBuf[0]);
                    if (true == CR->IsCommandValid())
                    {
                        // cout << " : Command Valid";
                        CR->set_value1(xfer.rxBuf[1]);
                        CR->set_value2(xfer.rxBuf[2]);
                        printf("valid %.2X %.2X %.2X", 
                            CR->get_command(), CR->get_value1(), CR->get_value2());
                    }
                    else
                    {
                        printf("INVALID %.2X %.2X %.2X", 
                            CR->get_command(), CR->get_value1(), CR->get_value2());
                    }
                }
                else
                {
                    cout << "DOES NOT match the size of a command "
                    << sizeof(CommandResponse)
                    << " : "
                    << xfer.rxCnt;
                }
                cout << "\n";
#if 0                
                for(int i = 0; i < xfer.rxCnt; i++)
                    cout << xfer.rxBuf[i];
                cout << "\n";
#endif                
            }
            /* else
            {
                cout << "No Data\n";
            } */

            //if (xfer.rxCnt > 0){
            //    cout << xfer.rxBuf;
            //}
    }
    }else
        cout << "Failed to open slave!!!\n";
}

void closeSlave() {
    gpioInitialise();
    cout << "Initialized GPIOs\n";

    xfer.control = getControlBits(slaveAddress, false);
    bscXfer(&xfer);
    cout << "Closed slave.\n";

    gpioTerminate();
    cout << "Terminated GPIOs.\n";
}


int getControlBits(int address /* max 127 */, bool open) {
    /*
    Excerpt from http://abyz.me.uk/rpi/pigpio/cif.html#bscXfer regarding the control bits:

    22 21 20 19 18 17 16 15 14 13 12 11 10 09 08 07 06 05 04 03 02 01 00
    a  a  a  a  a  a  a  -  -  IT HC TF IR RE TE BK EC ES PL PH I2 SP EN

    Bits 0-13 are copied unchanged to the BSC CR register. See pages 163-165 of the Broadcom 
    peripherals document for full details. 

    aaaaaaa defines the I2C slave address (only relevant in I2C mode)
    IT  invert transmit status flags
    HC  enable host control
    TF  enable test FIFO
    IR  invert receive status flags
    RE  enable receive
    TE  enable transmit
    BK  abort operation and clear FIFOs
    EC  send control register as first I2C byte
    ES  send status register as first I2C byte
    PL  set SPI polarity high
    PH  set SPI phase high
    I2  enable I2C mode
    SP  enable SPI mode
    EN  enable BSC peripheral
    */

    // Flags like this: 0b/*IT:*/0/*HC:*/0/*TF:*/0/*IR:*/0/*RE:*/0/*TE:*/0/*BK:*/0/*EC:*/0/*ES:*/0/*PL:*/0/*PH:*/0/*I2:*/0/*SP:*/0/*EN:*/0;

    int flags;
    if(open)
        flags = /*RE:*/ (1 << 9) | /*TE:*/ (1 << 8) | /*I2:*/ (1 << 2) | /*EN:*/ (1 << 0);
    else // Close/Abort
        flags = /*BK:*/ (1 << 7) | /*I2:*/ (0 << 2) | /*EN:*/ (0 << 0);

    return (address << 16 /*= to the start of significant bits*/) | flags;
}
