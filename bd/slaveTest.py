import ctypes
# import os
import time

print("test start")

#print ("%s" % (os.environ))
#print ("")

testlib = ctypes.cdll.LoadLibrary('/home/pi/pi-c/bd/slaveTest.so')
if None == testlib:
    print("testlib is NULL")
else:
    for i in [1,2,3,4,5]:
        testlib.main()
        print("Received [ %.2X %.2X %.2X ]" % 
            (testlib.get_Command(), testlib.get_Value1(), testlib.get_Value2()))
        time.sleep(0.6)

print("test end")

