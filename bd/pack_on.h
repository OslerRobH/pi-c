#ifdef _MSC_VER
#pragma pack(1)
#define PACKED_STRUCTURE /* nothing */
#endif

#ifdef __GNUC__
#define PACKED_STRUCTURE __attribute__(__packed__) {
#define PACKED_CLASS __attribute__(__packed__) {
#endif