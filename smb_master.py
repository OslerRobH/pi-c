from smbus import SMBus
import time

addr = 0x09 # bus address
bus = SMBus(1) # indicates /dev/ic2-1

while True:
    ts = time.gmtime()

    try:
        bus.write_byte(addr, 0xAA) # switch it on
        #print(time.strftime("%Y-%m-%d %H:%M:%S", ts))
        print ("PASS")

    except:
        print ("FAIL")
        #pass

    time.sleep(0.1)
